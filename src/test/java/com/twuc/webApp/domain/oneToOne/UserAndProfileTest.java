package com.twuc.webApp.domain.oneToOne;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class UserAndProfileTest extends JpaTestBase {
    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private UserProfileEntityRepository userProfileEntityRepository;

    @Test
    void should_save_user_and_profile() {
        // TODO
        //
        // 请书写一个测试实现如下的功能：
        //
        // Given 一个 UserEntity 和一个 UserProfileEntity。它们并没有持久化。
        // When 持久化 UserEntity
        // Then UserProfileEntity 也一同被持久化了。
        //
        // <--start--
        ClosureValue<Long> userEntityId = new ClosureValue<>();
        flushAndClear(em -> {
            UserEntity userEntity = new UserEntity();
            UserProfileEntity userProfileEntity = new UserProfileEntity("profile", "profile last name");
            userProfileEntity.setUserEntity(userEntity);
            em.persist(userEntity);
            userEntityId.setValue(userEntity.getId());
            userEntity.setUserProfileEntity(userProfileEntity);
        });

        flushAndClear(em -> {
            UserEntity userEntity = em.find(UserEntity.class, userEntityId.getValue());
            UserProfileEntity userProfileEntity = em.find(UserProfileEntity.class, 2L);
            assertNotNull(userEntity);
            assertNotNull(userProfileEntity);
        });
        // --end->
    }

    @Test
    void should_remove_parent_and_child() {
        // TODO
        //
        // 请书写一个测试：
        //
        // Given 一个持久化了的 UserEntity 和 UserProfileEntity
        // When 删除 UserEntity
        // Then UserProfileEntity 也被删除了
        //
        // <--start-
        ClosureValue<Long> userEntityId = new ClosureValue<>();
        ClosureValue<Long> userProfileId = new ClosureValue<>();

        flushAndClear(em -> {
            UserEntity userEntity = new UserEntity();
            UserProfileEntity userProfileEntity = new UserProfileEntity("profile", "profile last name");
            userProfileEntity.setUserEntity(userEntity);
            em.persist(userEntity);
            userEntityId.setValue(userEntity.getId());
            userEntity.setUserProfileEntity(userProfileEntity);
            em.persist(userProfileEntity);
            userProfileId.setValue(userProfileEntity.getId());

        });

        flushAndClear(em -> {
            UserEntity userEntity = em.find(UserEntity.class, userEntityId.getValue());
            em.remove(userEntity);
        });

        flushAndClear(em -> {
            UserEntity userEntity = em.find(UserEntity.class, userEntityId.getValue());
            UserProfileEntity userProfileEntity = em.find(UserProfileEntity.class, userProfileId.getValue());
            assertNull(userEntity);
            assertNull(userProfileEntity);
        });
        // --end->
    }
}